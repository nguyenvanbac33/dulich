<?php
  include 'connection.php';
  $col = 'col-md-4';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dịch vụ du lịch & đặt tour</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Alex+Brush" rel="stylesheet">

  <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
  <link rel="stylesheet" href="css/animate.css">

  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" href="css/magnific-popup.css">

  <link rel="stylesheet" href="css/aos.css">

  <link rel="stylesheet" href="css/ionicons.min.css">

  <link rel="stylesheet" href="css/bootstrap-datepicker.css">
  <link rel="stylesheet" href="css/jquery.timepicker.css">


  <link rel="stylesheet" href="css/flaticon.css">
  <link rel="stylesheet" href="css/icomoon.css">
  <link rel="stylesheet" href="css/style.css?version=1.1">
</head>
<body>

  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light scrolled awake" id="ftco-navbar">
    <div class="container">
      <a class="navbar-brand" href="index.php"><img src="img/logo.jpg" width="100px" height="100px" /></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

      <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item"><a href="index.php" class="nav-link">Trang chủ</a></li>
          <li class="nav-item"><a href="tour.php" class="nav-link">Tour</a></li>
          <li class="nav-item"><a href="hotel.php" class="nav-link">Khác sạn</a></li>
          <li class="nav-item"><a href="news.php" class="nav-link">Tin tức</a></li>
          <li class="nav-item"><a href="discount.php" class="nav-link">Khuyến mãi</a></li>
          <li class="nav-item"><a href="contact.php" class="nav-link">Liên hệ</a></li>
          <li class="nav-item"><a href="#footer" class="nav-link">About</a></li>
          <?php
            if (isset($_SESSION['user-username'])) {
              echo '<li class="nav-item"><a href="history.php" class="nav-link">Tour của tôi</a></li>';
              echo '<li class="nav-item cta"><a href="login.php?logout=1" onclick="return confirm(Bạn muốn đăng xuất?);" class="nav-link"><span>Hi, '.$_SESSION['user-name'].'</span></a></li>';
            } else {
              echo '<li class="nav-item cta"><a href="login.php" class="nav-link"><span>Login</span></a></li>';
            }
          ?>
        </ul>
      </div>
    </div>
  </nav>
  <div style="height: 50px"></div>