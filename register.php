<?php
include 'header.php';
if (isset($_POST['register'])) {
		$password = $_POST['password'];
		$name = $_POST['name'];
		$phone = $_POST['phone'];
		$address = $_POST['address'];
		$email = $_POST['email'];
		$sql = "INSERT INTO `account`(`username`, `password`, `name`, `phone`, `address`, `type`) VALUES ('$email','$password','$name','$phone','$address',1)";
		$query = $conn -> query($sql);
		if ($query) {
			echo "<script type='text/javascript'>alert('Đăng ký thành công');</script>";
			echo "<script>location.href='login.php';</script>";
		}else {
			echo "<script type='text/javascript'>alert('Đăng ký thất bại');</script>";
		}
	}
?>

<section class="ftco-section contact-section ftco-degree-bg">
	<div class="container">
		<div class="row d-flex mb-5 contact-info">
			<div class="col-md-12 mb-4">
				<center>
					<h3 style="padding: 50px">Đăng ký</h3>
					<form action="#" method="post" style="width: 40%">
						<div class="form-group">
							<input type="email" class="form-control" placeholder="Email" name="email" required>
						</div>
						<div class="form-group" style="margin-top: 50px">
							<input type="text" class="form-control" placeholder="Họ tên" name="name" required>
						</div>
						<div class="form-group" style="margin-top: 50px">
							<input type="text" class="form-control" placeholder="Địa chỉ" name="address" required>
						</div>
						<div class="form-group" style="margin-top: 50px">
							<input type="text" class="form-control" placeholder="Số điện thoại" name="phone" required>
						</div>
						<div class="form-group" style="margin-top: 50px">
							<input type="password" class="form-control" placeholder="Mật khẩu" name="password" required>
						</div>
						<div class="form-group" style="text-align: right;">
							<input type="submit" value="Đăng ký" name="register" class="btn btn-primary py-3 px-5">
						</div>
					</form>
				</center>
			</div>
		</div>
	</div>
</section>
<?php
include 'footer.php';
?>