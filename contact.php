<?php
include 'header.php';
if (isset($_POST['send'])) {
  $email = $_POST['email'];
  $name = $_POST['name'];
  $phone = $_POST['phone'];
  $title = $_POST['title'];
  $message = $_POST['message'];
  $date = getCurrentDate();
  $sql = "INSERT INTO `contact`(`email`, `name`, `phone`, `title`, `message`, `sent_date`) VALUES ('$email', '$name', '$phone', '$title', '$message', '$date')";
  $result = $conn -> query($sql);
  if ($result) {
    echo "<script type='text/javascript'>alert('Gửi thành công');</script>";
    echo "<script>location.href='index.php';</script>";
  }else {
    echo "<script type='text/javascript'>alert('Gửi thất bại');</script>";
  }
}
?>

<section class="ftco-section contact-section ftco-degree-bg">
  <div class="container">
    <div class="row d-flex mb-5 contact-info">
      <div class="col-md-12 mb-4">
        <h2 class="h4">Contact Information</h2>
      </div>
      <div class="w-100"></div>
      <div class="col-md-3">
        <p><span>Address:</span> 198 West 21th Street, Suite 721 New York NY 10016</p>
      </div>
      <div class="col-md-3">
        <p><span>Phone:</span> <a href="tel://1234567920">+ 1235 2355 98</a></p>
      </div>
      <div class="col-md-3">
        <p><span>Email:</span> <a href="mailto:info@yoursite.com">info@yoursite.com</a></p>
      </div>
      <div class="col-md-3">
        <p><span>Website</span> <a href="#">yoursite.com</a></p>
      </div>
    </div>
    <div class="row block-9">
      <div class="col-md-6 pr-md-5">
        <form action="#" method="post" id="contact">
          <div class="form-group">
            <input type="text" name="name" class="form-control" placeholder="Họ tên" required>
          </div>
          <div class="form-group">
            <input type="email" name="email" class="form-control" placeholder="Email" required>
          </div>
          <div class="form-group">
            <input type="mail" name="phone" class="form-control" placeholder="Số điện thoại" required>
          </div>
          <div class="form-group">
            <input type="text" name="title" class="form-control" placeholder="Tiêu đề" required>
          </div>
          <div class="form-group">
            <textarea name="message" form="contact" cols="30" rows="7" class="form-control" placeholder="Nội dung" required></textarea>
          </div>
          <div class="form-group">
            <input type="submit" value="Gửi tin nhắn" name="send" class="btn btn-primary py-3 px-5">
          </div>
        </form>

      </div>

      <div class="col-md-6" id="map"></div>
    </div>
  </div>
</section>

<?php
include 'footer.php';
?>