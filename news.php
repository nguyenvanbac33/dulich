<?php
  include 'header.php';
?>


    <section class="ftco-section bg-light">
      <div class="container">
        <div class="row d-flex">
          <?php
            $sql =  "SELECT * FROM `news` ORDER BY id desc";
            $query = $conn -> query($sql);
            while ($row = $query-> fetch_array()) {
              include 'news-item.php';
            }
          ?>
        </div>
      </div>
    </section>

<?php
  include 'footer.php';
?>