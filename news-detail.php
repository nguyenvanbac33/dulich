<?php
  include 'header.php';
  $id = $_GET['id'];
  $sql =  "SELECT * FROM `news` where id = ".$id;
  $query = $conn -> query($sql);
  $row = $query-> fetch_array();
?>
    <section class="ftco-section ftco-degree-bg">
      <div class="container">
        <div class="row">
          <div class="col-md-8 ftco-animate">
            <h2 class="mb-3"><?php echo $row['title']?></h2>
            <div><span class="icon-calendar"></span> <?php echo $row['pub_date']?></div>
            <h5><?php echo $row['desc']?></h5>
            <div  id="news-content"><?php echo $row['content']?></div>
          </div> <!-- .col-md-8 -->
          <div class="col-md-4 sidebar ftco-animate">
            <div class="sidebar-box ftco-animate">
              <h3>Tin tức mới nhất</h3>
              <?php
                $sql =  "SELECT * FROM `news` ORDER BY RAND() LIMIT 4";
                $query = $conn -> query($sql);
                while ($row = $query-> fetch_array()) {
                  ?>
                    <div class="block-21 mb-4 d-flex">
                      <a href="news-detail.php?id=<?php echo $row['id']?>" class="blog-img mr-4" style="background-image: url(<?php echo $row['image']?>);"></a>
                      <div class="text">
                        <h3 class="heading"><?php echo $row['title']?></h3>
                        <div class="meta">
                          <div><span class="icon-calendar"></span> <?php echo $row['pub_date']?></div>
                        </div>
                      </div>
                    </div>
                  <?php
                }
              ?>
            </div>
          </div>

        </div>
      </div>
    </section> <!-- .section -->

<?php
  include 'footer.php';
?>
<style type="text/css">
    #news-content{
        text-align: justify; 
        text-justify: inter-word;
    }
    #news-content img {
        width: 100% !important;
    }
</style>