<div class="col-sm <?php echo $col?> ftco-animate">
	<div class="destination">
		<a class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url(<?php echo $row['image']?>);">
		</a>
		<div class="text p-3">
			<div class="d-flex">
				<div>
					<h3 style="height: 100px; overflow: hidden;"><a><?php echo $row['name']?></a></h3>
					<div class="two">
						<span class="price"><?php echo formatPrice($row['price'])?></span>
					</div>
					<p class="rate">
						<?php
							include 'star.php';
						?>
					</p>
				</div>
			</div>
			<p><?php echo substr($row['short_desc'], 0, 50).'...';?></p>
			<p class="days"><span><?php echo $row['time']?></span></p>
			<hr>
			<p class="bottom-area d-flex">
				<span class="ml-auto"><a href="tour-detail.php?id=<?php echo $row['id']?>">Khám phá</a></span>
			</p>
		</div>
	</div>
</div>