<?php
include 'header.php';
if (isset($_POST['login'])) {
  $email = $_POST['email'];
  $password = $_POST['password'];
  $sql = "SELECT * FROM account where username = '$email' and password = '$password'";
  $query = $conn -> query($sql);
  if ($row = $query -> fetch_array()) {
    $_SESSION["user-username"] = $email;
    $_SESSION['user-name'] = $row['name'];
    echo "<script>location.href='index.php';</script>";
  }else {
    echo "<script type='text/javascript'>alert('Đăng nhập thất bại');</script>";
  }
}else if (isset($_GET['logout'])) {
  unset($_SESSION['user-username']);
  unset($_SESSION['user-name']);
  echo "<script>location.href='login.php';</script>";
}
?>

<section class="ftco-section contact-section ftco-degree-bg">
  <div class="container">
    <div class="row d-flex mb-5 contact-info">
      <div class="col-md-12 mb-4">
        <center>
          <h3 style="padding: 50px">Đăng nhập</h3>
          <form action="#" method="post" style="width: 40%">
            <div class="form-group">
              <input type="email" class="form-control" placeholder="Email" name="email" required>
            </div>
            <div class="form-group" style="margin-top: 50px">
              <input type="password" class="form-control" placeholder="Mật khẩu" name="password" required>
            </div>
            <div class="form-group" style="text-align: right;">
              <a href="register.php">Đăng ký</a>
            </div>
            <div class="form-group" style="text-align: right;">
              <input type="submit" value="Đăng nhập" name="login" class="btn btn-primary py-3 px-5">
            </div>
          </form>
        </center>
      </div>
    </div>
  </div>
</section>
<?php
include 'footer.php';
?>