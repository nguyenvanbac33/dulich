<div class="col-md-4 d-flex ftco-animate">
	<div class="blog-entry align-self-stretch">
		<a href="news-detail.php?id=<?php echo $row['id']?>" class="block-20" style="background-image: url('<?php echo $row['image']?>');"></a>
		<div class="text p-4 d-block">
			<span class="tag"><?php echo $row['title']?></span>
			<div class="meta mb-3">
				<div><?php echo $row['pub_date']?></div>
			</div>
		</div>
	</div>
</div>