<div class="col-sm <?php echo $col?> ftco-animate">
	<div class="destination">
		<div class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url(<?php echo $row['image']?>);">
		</div>
		<div class="text p-3">
			<div class="d-flex">
				<div>
					<h3 style="height: 50px; overflow: hidden;"><?php echo $row['name']?></h3>
					<div class="two">
						<span class="price per-price"><?php echo formatPrice($row['price'])?><small>/Ngày</small></span>
					</div>
					<p class="rate">
						<?php
							include 'star.php';
						?>
					</p>
				</div>
			</div>
			<hr>
			<p class="bottom-area d-flex">
				<span class="ml-auto"><a href="hotel-detail.php?id=<?php echo $row['id']?>">Xem ngay</a></span>
			</p>
		</div>
	</div>
</div>