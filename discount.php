<?php
  include 'header.php';
?>
    <section class="ftco-section bg-light">
      <div class="container">
        <div class="row d-flex">
          <?php
            $sql =  "SELECT * FROM `discount` ORDER BY id desc";
            $query = $conn -> query($sql);
            while ($row = $query-> fetch_array()) {
              ?>
              	<div class="col-md-4 d-flex ftco-animate">
					<div class="blog-entry align-self-stretch">
						<a class="block-20" style="background-image: url('<?php echo $row['image']?>');"></a>
						<div class="text p-4 d-block">
							<span class="tag" style="color: #000"><?php echo $row['name']?></span>
							<p class="tag"><?php echo $row['description']?></p>
							<div class="meta mb-3">
								<div><?php echo $row['date_start'].' - '.$row['date_end']?></div>
							</div>
						</div>
					</div>
				</div>
              <?php
            }
          ?>
        </div>
      </div>
    </section>

<?php
  include 'footer.php';
?>