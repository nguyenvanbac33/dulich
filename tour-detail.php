<?php
include 'header.php';
$id = $_GET['id'];
if (isset($_POST['register-tour'])) {
	if (!isset($_SESSION['user-username'])) {
		echo "<script type='text/javascript'>alert('Bạn phải đăng nhập trước');</script>";
		echo "<script>location.href='login.php';</script>";
	} else {
		$username = $_SESSION["user-username"];
		$date = $_POST['date'];
		$count = $_POST['count'];
		$child = $_POST['count-child'];
		$note = $_POST['note'];
		$pay_type = $_POST['pay_type'];
		$currentDate = getCurrentDate();
		$sql = "INSERT INTO `cart`(`order_date`, `status`, `username`, `tour_id`, `person`, `person_child`, `start_date`, `note`, pay_type) VALUES ('$currentDate', 0, '$username', '$id', '$count', '$child', '$date', '$note', '$pay_type')";
		$query = $conn -> query($sql);
		if ($query) {
			echo "<script type='text/javascript'>alert('Đặt tour thành công');</script>";
					// echo "<script>location.href='checkout.php';</script>";
		}else {
			echo "<script type='text/javascript'>alert('Đặt tour thất bại');</script>";
		}
	}
}
$id = $_GET['id'];
$sql =  "SELECT * FROM `tour` where id =".$id;
$query = $conn -> query($sql);
$row = $query-> fetch_array();
$sql = "SELECT url from image where id_item =".$id;
$query = $conn -> query($sql);
?>

<section class="ftco-section ftco-degree-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-md-12 ftco-animate">
						<div class="single-slider owl-carousel">
							<?php
							while ($row1 = $query-> fetch_array()) {
								?>
								<div class="item">
									<div class="hotel-img" style="background-image: url(<?php echo $row1['url']?>);"></div>
								</div>
								<?php
							}
							?>
						</div>
					</div>
					<div class="col-md-12 hotel-single mt-4 mb-5 ftco-animate">
						<h2><?php echo $row['name']?></h2>
						<p class="rate mb-5">
							<span class="loc"><i class="icon-time"></i> <?php echo $row['time']?></span><br/>
							<span class="star">
								<?php
								include 'star.php';
								?>
							</span><br/>
							<span class="loc"><i class="icon-money"></i> <?php echo formatPrice($row['price'])?></span><br/>
							<h5><?php echo $row['short_desc']?></h5>
						</p>
						<div id="content"><?php echo $row['desc']?></div>
					</div>
					<div class="col-md-12 pr-md-5">
						<form action="#" method="post">
							<div class="form-group">
								<input type="date" class="form-control" placeholder="Ngày khởi hành" name="date" required>
							</div>
							<div class="form-group">
								<input type="number" class="form-control" placeholder="Số người lớn" name="count" required>
							</div>
							<div class="form-group">
								<input type="number" class="form-control" placeholder="Số trẻ con" name="count-child" required>
							</div>
							<div class="form-group">
								<select name="pay_type">
									<option value="Chuyển khoản">Chuyển khoản</option>
									<option value="Tiền mặt">Tiền mặt</option>
								</select>
							</div>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Ghi chú" name="note">
							</div>
							
							<div class="form-group">
								<input type="submit" value="Đăng ký tour" name="register-tour" class="btn btn-primary py-3 px-5">
							</div>
						</form>

					</div>
				</div>

			</div>
		</div> <!-- .col-md-8 -->
	</div>
</div>
</section> <!-- .section -->
<?php
include 'footer.php';
?>

<style type="text/css">
	#content{
		text-align: justify; 
		text-justify: inter-word;
	}
	#content img {
		width: 100% !important;
	}
</style>