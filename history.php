<?php
include 'header.php';
if (isset($_GET['del'])) {
	$id = $_GET['del'];
	$sql = "UPDATE cart set status = -1 where id = $id";
	$query = $conn -> query($sql);
	if ($query) {
		echo "<script type='text/javascript'>alert('Hủy đơn đặt thành công');</script>";
		echo "<script>location.href='history.php';</script>";
	}else {
		echo "<script type='text/javascript'>alert('Hủy đơn đặt thất bại');</script>";
	}
}
?>
<section class="ftco-section ftco-degree-bg">
	<div class="container">
		<h3 style="padding: 50px; text-align: center;"><b>Tour của bạn</b></h3>

		<div class="checkout-right">
			<table  style="width: 100%">
				<thead>
					<tr>
						<th>Số thứ tự</th>	
						<th>Ngày đặt</th>
						<th>Tên tour</th>
						<th>Giá</th>
						<th>Người lớn</th>
						<th>Trẻ em</th>
						<th>Tổng giá</th>
						<th>Ngày khởi hành</th>
						<th>Thanh Toán</th>
						<th>Trạng thái</th>
						<th>Hủy</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$username = $_SESSION['user-username'];
					$sql = "SELECT a.*, b.name, b.price FROM cart a inner join tour b on a.tour_id = b.id where username = '$username' order by id desc";
					$query = $conn -> query($sql);
					$i=1;
					while ($row = $query -> fetch_array()) {
						?>
						<tr class="rem1" style="height: 50px">
							<td class="invert"><?php echo ($i++)?></td>
							<td class="invert"><?php echo $row['order_date']?></td>
							<td class="invert" style="width: 200px"><?php echo $row['name']?></td>
							<td class="invert"><?php echo formatPrice($row['price'])?></td>
							<td class="invert"><?php echo $row['person']?></td>
							<td class="invert"><?php echo $row['person_child']?></td>
							<td class="invert"><?php echo formatPrice($row['price'] * ($row['person'] + $row['person_child']))?></td>
							<td class="invert"><?php echo $row['start_date']?></td>
							<td class="invert"><?php echo $row['pay_type']?></td>
							<td class="invert"><?php echo getStatus($row['status'])?></td>
							<td class="invert">
								<?php
								if ($row['status'] == 0) {
									?>
									<a href="?del=<?php echo $row['id']?>" onclick="return confirm('Bạn muốn hủy đơn đặt?');">
										<i class="icon-delete"></i>
									</a>
									<?php
								}
								?>
							</td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</section>
<?php
include 'footer.php';
?>