<?php 
include 'header.php';
$name = "";
$arrChecked = array(false, false, false, false, false);
if (isset($_POST['search'])) {
	$name = $_POST['name'];
	for ($i=0; $i < 5; $i++) { 
		if (isset($_POST[$i])) {
			$arrChecked[$i] = $_POST[$i];
		}
	}
}
?>
<section class="ftco-section ftco-degree-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 sidebar">
				<div class="sidebar-wrap bg-light ftco-animate">
					<h3 class="heading mb-4">Tìm kiếm khách sạn</h3>
					<form method="post">
						<div class="fields">
							<div class="form-group">
								<input type="text" class="form-control" name="name" value="<?php echo $name?>" placeholder="Tên khách sạn">
							</div>
							<h3 class="heading mb-4">Điểm đánh giá</h3>
							<?php
							for ($i= 4; $i >= 0; $i--) { 
								$str = '';
								$checked = '';
								if ($arrChecked[$i] == true) {
									$checked = 'checked';
								}
								for ($j=0; $j < 5; $j++) { 
									if ($j <= $i) {
										$str = $str.'<i class="icon-star"></i>';
									} else {
										$str = $str.'<i class="icon-star-o"></i>';
									}
								}
								echo '
								<div class="form-check">
								<input type="checkbox" name="'.$i.'" '.$checked.' class="form-check-input" id="exampleCheck1">
								<label class="form-check-label" for="exampleCheck1">
								<p class="rate">
								<span>'.$str.'</span>
								</p>
								</label>
								</div>
								';
							}
							?>
							<div class="form-group">
								<input type="submit" name="search" value="Search" class="btn btn-primary py-3 px-5">
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="row">
					<?php
					$star = '';
					for ($i=1; $i <= 5; $i++) { 
						if ($arrChecked[$i - 1] == true) {
							if ($star == '') {
								$star = $star . $i;
							} else {
								$star = $star . ',' . $i;
							}
						}
					}
					if ($star != '') {
						$star = 'and star in('.$star.')';
					}
					$sql =  "SELECT a.*, (SELECT url from image where id_hotel = a.id limit 1) as image FROM `hotel` a  where name like '%".$name."%' ".$star." order by id desc";
					$query = $conn -> query($sql);
					while ($row = $query-> fetch_array()) {
						include 'hotel-item.php';
					}
					?>
				</div>
			</div> <!-- .col-md-8 -->
		</div>
	</div>
</section> <!-- .section -->
<?php include 'footer.php'; ?>