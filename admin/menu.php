<li class="nav-item <?php if($page == 'user') echo 'active' ?>">
    <a class="nav-link" href="?page=user">
	    <i class="material-icons">person</i>
	   	<p>Quản trị</p>
    </a>
</li>
<li class="nav-item <?php if($page == 'guess') echo 'active' ?>">
	<a class="nav-link" href="?page=guess">
		<i class="material-icons">group</i>
		<p>Khách hàng</p>
	</a>
</li>
<li class="nav-item <?php if($page == 'guide') echo 'active' ?>">
	<a class="nav-link" href="?page=guide">
		<i class="material-icons">group</i>
		<p>Hướng dẫn viên</p>
	</a>
</li>
<li class="nav-item <?php if($page == 'tour') echo 'active' ?>">
	<a class="nav-link" href="?page=tour">
		<i class="material-icons">group</i>
		<p>Tour</p>
	</a>
</li>
<li class="nav-item <?php if($page == 'hotel') echo 'active' ?>">
	<a class="nav-link" href="?page=hotel">
		<i class="material-icons">group</i>
		<p>Hotel</p>
	</a>
</li>
<li class="nav-item <?php if($page == 'request') echo 'active' ?>">
	<a class="nav-link" href="?page=request">
		<i class="material-icons">group</i>
		<p>Đơn đặt tour</p>
	</a>
</li>
<li class="nav-item <?php if($page == 'news') echo 'active' ?>">
	<a class="nav-link" href="?page=news">
		<i class="material-icons">credit_card</i>
		<p>Tin tức</p>
	</a>
</li>
<li class="nav-item <?php if($page == 'discount') echo 'active' ?>">
	<a class="nav-link" href="?page=discount">
		<i class="material-icons">credit_card</i>
		<p>Khuyến mãi</p>
	</a>
</li>
<li class="nav-item <?php if($page == 'contact') echo 'active' ?>">
	<a class="nav-link" href="?page=contact">
		<i class="material-icons">credit_card</i>
		<p>Liên hệ</p>
	</a>
</li>
<li class="nav-item <?php if($page == 'report') echo 'active' ?>">
	<a class="nav-link" href="?page=report">
		<i class="material-icons">credit_card</i>
		<p>Thống kê</p>
	</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="../admin/login.php?logout=logout">
		<i class="material-icons">exit_to_app</i>
		<p>Đăng xuất</p>
	</a>
</li>