<script language="javascript" src="../admin/ckeditor/ckeditor.js"></script>
<?php
    if (isset($_POST['submit-edit-row'])) {
        $newname = "";
        if ($_FILES['image']['name'] != "") {
            // Lấy tên gốc của file
            $filename = $_FILES['image']['name'];
            $newname = "img/news/" . $filename;
            // Lưu ảnh vào thư mục
            move_uploaded_file($_FILES['image']['tmp_name'], "../".$newname);
        }
        $id = $_POST['id'];
        $name = $_POST['name'];
        $date_start = $_POST['date_start'];
        $date_end = $_POST['date_end'];
        $desc = $_POST['desc'];
        $sql = "UPDATE `discount` SET `name`='$name',`description`='$desc', date_end='$date_end', date_start='$date_start' WHERE `id`='$id'";
        if ($newname != "") {
            $sql = "UPDATE `discount` SET image='$newname',`name`='$name',`description`='$desc', date_end='$date_end', date_start='$date_start' WHERE `id`='$id'";
        }
        $result = $conn -> query($sql);
        if ($result == '') {
            echo "<script type='text/javascript'>alert('Update fail');</script>";
        }else{
            echo "<script type='text/javascript'>alert('Update success');</script>";
            echo '<meta http-equiv="refresh" content="0">';
        }
    }
?>
<div id="update-row" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Sửa Khuyến Mãi</h4>
            </div>
            <div class="modal-body">
            	<?php
						if(isset($_POST['edit'])){
							$id = $_POST['edit'];
							$sql = "select * from discount where id = '$id'";
							$query = $conn -> query($sql);
							$row = $query -> fetch_array();
				?>
                <form id="fr-add-alphabet" method="post" enctype="multipart/form-data">
                	<div class="form-group">
                        <label>Mã khuyến mãi</label> <input
                            type="text" class="form-control add-control" value="<?php echo $row['id'];?>" name="id" readonly required>
                    </div>
                    <div class="form-group">
                        <label>Tên khuyến mãi</label> <input
                            type="text" maxlength="100" class="form-control add-control" value="<?php echo $row['name'];?>" name="name" required>
                    </div>
                    <div class="form-group">
                        <label>Mô tả</label>
                        <br/>
                        <textarea name="desc" maxlength="200" class="form-control add-control" required rows="3"><?php echo $row['description'];?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Thời gian bắt đầu</label> <input
                            type="date" class="form-control add-control" name="date_start" value="<?php echo $row['date_start'];?>" required>
                    </div>
                    <div class="form-group">
                        <label>Thời gian kết thúc</label> <input
                            type="date" class="form-control add-control" name="date_end" value="<?php echo $row['date_end'];?>" required>
                    </div>
                    <div class="form-group">
                        <label>Hình ảnh</label>
                    </div>
                    <input accept="image/*" type="file" name="image"/>
                    <div class="form-group" style="text-align: right;">
                        <input type="submit" class="btn btn-primary" name="submit-edit-row" value="Ok"/>
                    </div>
                    <script type="text/javascript">
						$('#update-row').modal('show');
					</script>
                </form>
                <?php 
					}
				?>
            </div>
        </div>

    </div>
</div>