<script language="javascript" src="../admin/ckeditor/ckeditor.js"></script>
<?php
    if (isset($_POST['add-row'])) {
        $newname = "";
        if (isset($_FILES['image'])) {
            // Lấy tên gốc của file
            $filename = $_FILES['image']['name'];
            $newname = "img/news/" . $filename;
            // Lưu ảnh vào thư mục
            move_uploaded_file($_FILES['image']['tmp_name'], "../".$newname);
        }
        $name = $_POST['name'];
        $date_start = $_POST['date_start'];
        $date_end = $_POST['date_end'];
        $desc = $_POST['desc'];
        $sql = "INSERT INTO `discount`(`name`, `description`, date_start, `date_end`, image) VALUES ('$name','$desc','$date_start', '$date_end', '$newname')";
        $result = $conn -> query($sql);
        if ($result == '') {
            echo "<script type='text/javascript'>alert('Insert fail');</script>";
        }else{
            echo "<script type='text/javascript'>alert('Insert success');</script>";
            echo '<meta http-equiv="refresh" content="0">';
        }
    }
?>
<div id="insert" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Thêm Khuyến Mãi</h4>
            </div>
            <div class="modal-body">
                <form id="fr-add-alphabet" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Tên Khuyến Mãi</label> <input
                            type="text" maxlength="100" class="form-control add-control" name="name" required>
                    </div>
                    <div class="form-group">
                        <label>Mô tả</label>
                        <br/>
                        <textarea class="form-control add-control" rows="3" required name="desc"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Thời gian bắt đầu</label> <input
                            type="date" class="form-control add-control" name="date_start" required>
                    </div>
                    <div class="form-group">
                        <label>Thời gian kết thúc</label> <input
                            type="date" class="form-control add-control" name="date_end" required>
                    </div>
                    <div class="form-group">
                        <label>Hình ảnh</label>
                    </div>
                    <input accept="image/*" type="file" name="image" required />
                    <div class="form-group" style="text-align: right;">
                        <input type="submit" class="btn btn-primary" name="add-row" value="Ok"/>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>