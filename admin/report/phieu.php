<?php
    require('PDFPhieu.php');
    class PDF extends PDF_MySQL_Table
    {
        function Header()
        {
            // Title
            $this->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
            $this->SetFont('DejaVu','',18);
            $this->Cell(0,6,'Báo cáo danh thu',0,1,'C');
            $this->Ln(10);
            // Ensure table header is printed
            parent::Header();
        }
    }

    // Connect to database
    include('../../connection.php');
    $pdf = new PDF();
    $pdf->AddPage();
    $timeF = "";
    $timeT = "";
    if (isset($_GET['time-from'])) {
        $timeF = $_GET['time-from'];
    }
    if (isset($_GET['time-to'])) {
        $timeT = $_GET['time-to'];
    }
    $pdf->Cell(0,10,'Thời gian: '.$timeF.' đến '.$timeT,0,0,'L');
    if ($timeF != "") {
            $timeF = date('Ymd', strtotime($timeF));
    }
    if ($timeT != "") {
            $timeT = date('Ymd', strtotime($timeT));
    }
    $pdf->Ln();

    $sql = "SELECT SUBSTRING(b.name, 1, 25) as 'Tour', b.price as 'Giá', (a.person + a.person_child) as 'Số người', ((a.person + a.person_child) * b.price) as 'Tổng tiền' FROM cart a inner join tour b on a.tour_id = b.id where (DATE_FORMAT(STR_TO_DATE(`order_date`, '%d/%m/%Y'), '%Y%m%d') >= '$timeF' and DATE_FORMAT(STR_TO_DATE(`order_date`, '%d/%m/%Y'), '%Y%m%d') <= '$timeT') and a.status > -1";
    // First table: output all columns
    $pdf->Table($conn,$sql);
    $pdf->Output();
?>