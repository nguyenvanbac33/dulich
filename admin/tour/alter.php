<?php
if (isset($_POST['submit-edit-row'])) {
    $name = $_POST['name'];
    $desc = $_POST['desc'];
    $price = $_POST['price'];
    $id = $_POST['id'];
    $time = $_POST['time'];
    $short_desc = $_POST['short_desc'];
    $star = $_POST['star'];

    $sql = "UPDATE `tour` SET `name`='$name',`time`='$time',`price`=$price,`desc`='$desc',`short_desc`='$short_desc',`star`='$star' WHERE id = $id";
    $result = $conn -> query($sql);
    if ($result == '') {
        echo "<script type='text/javascript'>alert('Update fail');</script>";
    }else{
        if (isset($_FILES['images']))
        {
            $myFile = $_FILES['images'];
            $fileCount = count($myFile["name"]);
            if ($myFile['name'][0]) {
                echo $fileCount;
                $sql = "DELETE FROM image WHERE id_item = $id";
                $conn -> query($sql);
                for ($i = 0; $i < $fileCount; $i++) {
                    $uploadfile = 'img/tour/'.$myFile['name'][$i];
                    move_uploaded_file($myFile['tmp_name'][$i], "../".$uploadfile);
                    $sql = "INSERT INTO `image`(`id_item`, `url`) VALUES ($id,'$uploadfile')";
                    $conn->query($sql);
                }
            }
        }
        echo "<script type='text/javascript'>alert('Update success');</script>";
        echo '<meta http-equiv="refresh" content="0">';
    }
}
?>

<div id="update-row" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Sửa thông tin tour</h4>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data">
                   <?php
                   if(isset($_POST['edit'])){
                       $id = $_POST['edit'];
                       $sql = "select * from tour where id = '$id'";
                       $query = $conn -> query($sql);
                       $row = $query -> fetch_array();
                       ?>

                       <div class="form-group">
                        <label>Mã tour</label> <input
                        type="text" maxlength="100" class="form-control add-control" value="<?php echo $id?>" name="id" readOnly>
                    </div>
                    <div class="form-group">
                        <label>Tên tour</label> <input
                        type="text" maxlength="100" class="form-control add-control" value="<?php echo $row['name']?>" name="name" required>
                    </div>
                    <div class="form-group">
                        <label>Thời gian</label>
                        <br/>
                        <input type="text" class="form-control add-control" value="<?php echo $row['time']?>" required name="time">
                    </div>
                    <div class="form-group">
                        <label>Giá</label>
                        <br/>
                        <input type="number" class="form-control add-control" value="<?php echo $row['price']?>" required name="price">
                    </div>
                    <div class="form-group">
                        <label>Mô tả ngắn</label>
                        <br/>
                        <input type="text" class="form-control add-control" value="<?php echo $row['short_desc']?>" required name="short_desc">
                    </div>
                    <div class="form-group">
                        <label>Mô tả</label>
                        <br/>
                        <textarea name="desc"  id="edit-content"><?php echo $row['desc']?></textarea>
                        <script type="text/javascript">CKEDITOR.replace('edit-content'); </script>
                    </div>
                    <div class="form-group">
                        <label>Đánh giá</label>
                        <br/>
                        <input type="number" class="form-control add-control" min="1" max="5" value="<?php echo $row['star']?>" required name="star">
                    </div>
                    <div class="form-group">
                        <label>Hình ảnh</label>
                    </div>
                    <input accept="image/*" type="file" name="images[]" multiple />

                    <div class="form-group" style="text-align: right;">
                        <input type="submit" class="btn btn-primary" name="submit-edit-row" value="Ok"/>
                    </div>
                    <script type="text/javascript">
                      $('#update-row').modal('show');
                  </script>
              </form>
              <?php 
          }
          ?>
      </div>
  </div>

</div>
</div>