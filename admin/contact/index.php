<?php
  function rows($row){
    echo "<tr>
            <td>".$row['name']."</td>
            <td>".$row['email']."</td>
            <td>".$row['phone']."</td>
            <td>".$row['title']."</td>
            <td>".$row['message']."</td>
            <td>".$row['sent_date']."</td>
          </tr>";
  }
?>
<div class="table-responsive">
  <table class="table table-hover">
    <thead class="text-primary">
      <th style="font-size: 15pt">
        Người gửi
      </th>
      <th style="font-size: 15pt">
        Email
      </th>
      <th style="font-size: 15pt">
        Số điện thoại
      </th>
      <th style="font-size: 15pt">
        Tiêu đề
      </th>
      <th style="font-size: 15pt">
        Nội dung
      </th>
      <th style="font-size: 15pt">
        Ngày gửi
      </th>
    </thead>
    <tbody>
      <?php
        $sql =  "select * from contact order by id desc";
        if (isset($_POST['key'])) {
          $key = $_POST['key'];
          $sql = $sql . " and(title like '%$key%' or message like '%$key%')";
        }
        $query = $conn -> query($sql);
        while ($row = $query-> fetch_array()) {
          rows($row);
        }
      ?>
    </tbody>
  </table>
</div>
