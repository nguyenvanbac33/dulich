
<?php
function render_row($row){
	$image = $row['image'];
	$name = $row['name'];
	$price = formatPrice($row['price']);
	$count = $row['count'];
	$size = $row['size'];
	echo "<tr>
	<td style=width:150px>
	<img  width=100 height=100 src='../$image'>
	</td>
	<td>
	$name
	</td>
	<td>
	$size
	</td>
	<td>
	$count
	</td>
	<td>
	$price
	</td>
	</tr>";
}
if (isset($_POST['confirm'])) {
	$id = $_POST['confirm'];
	$sql = "UPDATE cart set status = (status + 1) WHERE id = $id";
	if ($conn -> query($sql)) {
		echo "<script type='text/javascript'>alert('Xác nhận thành công');</script>";
	}else{
		echo "<script type='text/javascript'>alert('Xác nhận thất bại');</script>";
	}
}else if (isset($_POST['cancel'])) {
	$id = $_POST['cancel'];
	$sql = "UPDATE cart set status = -1 WHERE id = $id";
	if ($conn -> query($sql)) {
		echo "<script type='text/javascript'>alert('Huỷ thành công');</script>";
	}else{
		echo "<script type='text/javascript'>alert('Huỷ thất bại');</script>";
	}
}

$sql = "SELECT a.*, b.*, c.name as tour, c.price FROM cart a inner join account b on a.username = b.username inner join tour c on a.tour_id = c.id order by id desc";
$query = $conn -> query($sql);
while ($row = $query -> fetch_array()) {
	?>
	<div>
		<h3 style="color: red">
			<b>Người đặt: <?php echo $row['name']?></b>
		</h3>
		<h5>Địa chỉ người đặt: <?php echo $row['address']?></h5>
		<h5>SDT: <?php echo $row['phone']?></h5>
		<h5>Tên tour: <?php echo $row['tour']?></h5>
		<h5>Giá: <?php echo formatPrice($row['price'])?></h5>
		<h5>Số người lớn: <?php echo $row['person']?></h5>
		<h5>Số trẻ em: <?php echo $row['person_child']?></h5>
		<h5>Tổng giá: <?php echo formatPrice($row['price'] * ($row['person'] + $row['person_child']))?></h5>
		<h5>Ngày đặt: <?php echo $row['order_date']?></h5>
		<h5>Ngày khởi hành: <?php echo $row['start_date']?></h5>
		<h5 style="color: red"><?php echo getStatus($row['status'])?></h5>
		
		<?php
		if ($row['status'] < 2 && $row['status'] >= 0) {
			?>
			<form method="post">
				<div class="form-group" style="text-align: right;">
					<button type="submit" class="btn btn-primary" name="confirm" value="<?php echo $row['id']?>">
						<?php
						echo getStatusAction($row['status']);
						?>
					</button>
					<button style="margin-left: 10px;" type="submit" value="<?php echo $row['id']?>" name="cancel" id='add' class='btn btn-primary'>Huỷ</button>
				</div>
			</form>
			<?php
		}
		?>
		<hr/>
	</div>
	<?php
}
?>
