<?php
	if (isset($_POST['submit-edit-row'])) {
		$id = $_POST['id'];
		$name = $_POST['name'];
		$phone = $_POST['phone'];
		$address = $_POST['address'];
		$sql = "UPDATE `hdvien` SET `name`='$name',`phone`='$phone',`address`='$address' WHERE `id`='$id'";
		$result = $conn -> query($sql);
		if ($result == '') {
			echo "<script type='text/javascript'>alert('Update fail');</script>";
		}else{
			echo "<script type='text/javascript'>alert('Update success');</script>";
			echo '<meta http-equiv="refresh" content="0">';
		}
	}
?>
<div id="update-row" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Cập Nhập Hướng Dẫn Viên</h4>
			</div>
			<div class="modal-body">
				<form method="post">
					<?php
						if(isset($_POST['edit'])){
							$id = $_POST['edit'];
							$sql = "select * from hdvien where id = '$id'";
							$query = $conn -> query($sql);
							$row = $query -> fetch_array();
					?>
					<div class="form-group">
						<label>Mã hướng dẫn viên</label> <input
							type="text" class="form-control add-control" name="id" value="<?php echo $row['id'];?>" readonly required>
					</div>
					<div class="form-group">
						<label>Họ tên</label> <input
							type="text" class="form-control add-control" name="name" value="<?php echo $row['name'];?>" required>
					</div>
					<div class="form-group">
						<label>Địa chỉ</label> <input
							type="text" class="form-control add-control" name="address" value="<?php echo $row['address'];?>" required>
					</div>
					<div class="form-group">
						<label>Số điện thoại</label> <input
							type="text" class="form-control add-control" name="phone" value="<?php echo $row['phone'];?>" required>
					</div>
					<div class="form-group" style="text-align: right;">
						<input type="submit" class="btn btn-primary" name="submit-edit-row" value="Ok"/>
					</div>
					<script type="text/javascript">
						$('#update-row').modal('show');
					</script>
					<?php 
						}
					?>
				</form>
			</div>
		</div>

	</div>
</div>