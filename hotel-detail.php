<?php
include 'header.php';
$id = $_GET['id'];
$sql =  "SELECT * FROM `hotel` where id =".$id;
$query = $conn -> query($sql);
$row = $query-> fetch_array();
$sql = "SELECT url from image where id_hotel =".$id;
$query = $conn -> query($sql);
?>

<section class="ftco-section ftco-degree-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-md-12 ftco-animate">
						<div class="single-slider owl-carousel">
							<?php
							while ($row1 = $query-> fetch_array()) {
								?>
								<div class="item">
									<div class="hotel-img" style="background-image: url(<?php echo $row1['url']?>);"></div>
								</div>
								<?php
							}
							?>
						</div>
					</div>
					<div class="col-md-12 hotel-single mt-4 mb-5 ftco-animate">
						<h2><?php echo $row['name']?></h2>
						<p class="rate mb-5">
							<span class="loc"><i class="icon-map"></i> <?php echo $row['address']?></span><br/>
							<span class="star">
								<?php
								include 'star.php';
								?>
							</span><br/>
							<span class="loc"><i class="icon-money"></i> <?php echo formatPrice($row['price'])?></span><br/>
							<span class="loc"><i class="icon-phone"></i> <?php echo $row['phone']?></span><br/>
						</p>
						<p><?php echo $row['desc']?></p>
					</div>
					<div class="col-md-12 hotel-single ftco-animate mb-5 mt-5">
						<h4 class="mb-4">Related Hotels</h4>
						<div class="row">
							<?php
							$sql = "SELECT a.*, (SELECT url from image where id_hotel = a.id limit 1) as image FROM `hotel` as a  ORDER BY RAND() LIMIT 4";
							$query = $conn -> query($sql);
							while ($row = $query-> fetch_array()) {
								include 'hotel-item.php';
							}
							?>
						</div>
					</div>

				</div>
			</div> <!-- .col-md-8 -->
		</div>
	</div>
</section> <!-- .section -->
<?php
include 'footer.php';
?>